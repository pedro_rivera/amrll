<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Amarello
 */

get_header();

$queried_object = get_queried_object();
$tax = $queried_object->taxonomy;
$tax_name = $queried_object->name;
$tax_term = $queried_object->slug;
$terms = get_terms('secao');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">


		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php echo $tax_name; ?></h1>
			</header><!-- .page-header -->

			<?php
			if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
				$count = count( $terms );
				$i = 0;
				$term_list = '<nav class="secoes">';
				foreach ( $terms as $term ) {
					$i++;
					if($term == $queried_object) {
						$class = 'secao active';
					} else {
						$class = 'secao ';
					}
						$term_list .= '<a href="' . esc_url( get_term_link( $term ) ) . '" alt="' . esc_attr( sprintf( __( 'View all post filed under %s', 'amarello' ), $term->name ) ) . '" class="'.$class.'">' . $term->name . '</a>';
					if ( $count != $i ) {
						$term_list;
					}
					else {
						$term_list .= '</nav>';
					}
				}
				echo $term_list;
			}

			echo do_shortcode('[ajax_load_more loading_style="infinite classic" container_type="div" post_type="criacao" posts_per_page="12" order="ASC" orderby="menu_order" scroll_distance="-420" button_label="Outras criações" button_loading_label="Carregando criações" button_done_label="Todas as criações exibidas" transition_container_classes="criacoes" archive="true" taxonomy="'. $tax .'" taxonomy_terms="'. $tax_term .'" taxonomy_operator="IN"]');
		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
