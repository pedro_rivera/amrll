if(document.querySelector('body.archive.category')) {


	let newsletter = document.querySelector('.newsletter.block');
	if (newsletter) {
		let anterior = newsletter.previousElementSibling;
		let alm = anterior.querySelector('.alm-listing');
		// alm.append(newsletter);
		console.log(alm);
	}
}

if(document.querySelector('body.single-product')) {


    // sales product trick
    let onsale = document.querySelector('div.type-product > .onsale');
    if(onsale) {
        let proximo = onsale.nextElementSibling;
        proximo.append(onsale);
    }
}

if(document.querySelector('body.single-post')) {

    // sponsored content trick
        
    let patrocinado = document.querySelector('#patrocinado');
    if(patrocinado) {
        let proximo = patrocinado.nextElementSibling;
        proximo.append(patrocinado);
    }
}

// menu responsivo
let menuToggle = document.querySelector('.menu-toggle');
let menuTarget = document.querySelector('#masthead > nav');

menuToggle.addEventListener('click', () => {
	document.querySelector('body').classList.toggle('open-menu');
});
menuTarget.addEventListener('click', (e) => {
	if (e.target == e.currentTarget) { // prevent children clicks to trigger this event
		document.querySelector('body').classList.remove('open-menu');
	}
})

// hide first-payment-date if empty
if (document.querySelector('p.first-payment-date')) {
	if (document.querySelector('p.first-payment-date').innerText.length == 0) {
		document.querySelector('p.first-payment-date').remove();
	}
}

if(document.querySelector('.fswp_variable_installment_calculation')) {
	let parcelas = document.querySelector('.fswp_variable_installment_calculation'),
		price = document.querySelector('.entry-summary > .price');
		price.after(parcelas);
}

// login interaction
let loginTrigger = document.querySelector('.login-trigger');

if (loginTrigger) {
	loginTrigger.addEventListener('click', () => {
		document.querySelector('body').classList.toggle('open-login');
	});
	let overlay = document.querySelector('.login-overlay');
	loginTrigger.parentNode.parentNode.append(overlay);
	overlay.addEventListener('click', () => {
		document.querySelector('body').classList.toggle('open-login');
	}) 
}

// interação da busca do cabeçalho
let buscaTitle = document.querySelector('#masthead .widget.busca .widget-title');

buscaTitle.addEventListener('click', () => {
	document.querySelector('body').classList.add('open-search');
	document.querySelector('#masthead form.searchandfilter').addEventListener('click', (e) => {
		if (e.target == e.currentTarget) { // prevent children clicks to trigger this event
			document.querySelector('body').classList.remove('open-search');
		}
	})
});

// disable form autofill
if(document.querySelector('.woocommerce-form-login')) {
	let loginForm = document.querySelector('.woocommerce-form-login');
	loginForm.setAttribute('autocomplete', 'off');
}


(function ($) {
	if ($(".slideshow.imagens .slider").length > 0) {
		$(".slideshow.imagens .slider").slick({
			infinite: true,
			slidesToShow: 1,
			centerMode: true,
			centerPadding: "0",
			variableWidth: true,
			arrows: false,
			swipeToSlide: true,
			responsive: [{
				breakpoint: 1024,
				settings: {
					variableWidth: false,
				},
			}, ],
		});

		$(".foto").click(function (e) {
			$(this).preventDefault;
			$(this).stopPropagation;
		});

		$(".foto .prev").click(function () {
			$(".slideshow.imagens .slider").slick("slickPrev");
		});
		$(".foto .next").click(function () {
			$(".slideshow.imagens .slider").slick("slickNext");
		});
	}

	if ($('.slideshow.loop .slider').length > 0) {
		$('.slideshow.loop .slider').each(function (idx, item) {
			var carouselId = "carousel" + idx;
			this.id = carouselId;
			$(this).slick({
				slide: "#" + carouselId + " .card",
				arrows: true,
				prevArrow: $("#" + carouselId + "+ .slider-nav .prev"),
				nextArrow: $("#" + carouselId + "+ .slider-nav .next"),
				infinite: false,
				slidesToShow: 4,
				swipeToSlide: true,
				responsive: [{
						breakpoint: 1024,
						settings: {
							slidesToShow: 3,
						}
					},
					{
						breakpoint: 800,
						settings: {
							slidesToShow: 2,
						}
					},
					{
						breakpoint: 360,
						settings: {
							slidesToShow: 1,
						}
					},
				]
			});
		});
	}


	if ($('.tres.loop .slider').length > 0) {
		$('.tres.loop .slider').each(function (idx, item) {
			var carouselId = "carousel" + idx;
			this.id = carouselId;
			$(this).slick({
				slide: "#" + carouselId + " .card",
				arrows: true,
				prevArrow: $("#" + carouselId + "+ .slider-nav .prev"),
				nextArrow: $("#" + carouselId + "+ .slider-nav .next"),
				infinite: false,
				slidesToShow: 3,
				swipeToSlide: true,
				responsive: [{
						breakpoint: 960,
						settings: {
							slidesToShow: 2,
							infinite: true,
						}
					},
					{
						breakpoint: 640,
						settings: {
							slidesToShow: 1,
						}
					},
				]
			});
		});
	}

	if ($('section.related.products ul.products').length > 0) {
		$("section.related.products ul.products").each(function (idx, item) {
			var carouselId = "carousel" + idx;
			this.id = carouselId;
			$(this).slick({
				slide: "#" + carouselId + " .product",
				arrows: true,
				prevArrow: $("#" + carouselId + "+ .slider-nav .prev"),
				nextArrow: $("#" + carouselId + "+ .slider-nav .next"),
				infinite: false,
				slidesToShow: 3,
				swipeToSlide: true,
				responsive: [{
						breakpoint: 800,
						settings: {
							slidesToShow: 2,
						},
					},
					{
						breakpoint: 360,
						settings: {
							slidesToShow: 1,
						},
					},
				],
			});
		});
	}

	jQuery(document).ready(function ($) {
		$('.product-quantity').on('click', '.up', function (e) {
			$input = $(this).siblings('.quantity').children('input.qty');
			var val = parseInt($input.val());
			var step = $input.attr('step');
			step = 'undefined' !== typeof (step) ? parseInt(step) : 1;
			$input.val(val + step).change();
		});

		$('.product-quantity').on('click', '.down',
			function (e) {
				$input = $(this).siblings('.quantity').children('input.qty');
				var val = parseInt($input.val());
				var step = $input.attr('step');
				step = 'undefined' !== typeof (step) ? parseInt(step) : 1;
				if (val > 0) {
					$input.val(val - step).change();
				}
			});
	});
})(jQuery);