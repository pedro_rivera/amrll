<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Amarello
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">


		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php echo post_type_archive_title();?></h1>
			</header><!-- .page-header -->

			<?php

			echo do_shortcode('[ajax_load_more loading_style="infinite fading-circles" container_type="div" css_classes="edicoes" post_type="edicao" posts_per_page="10" meta_key="numero" meta_value="" meta_compare="IN" meta_type="NUMERIC" orderby="meta_value_num" scroll_distance="-100%" button_label="Edições anteriores" button_loading_label="Carregando edições" archive="true"]');
		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();