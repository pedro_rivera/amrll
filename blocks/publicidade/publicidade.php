<?php

/**
 * Block de Destaque Fixo
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'publicidade-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'publicidade';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$pub = get_field('publicidade');
    $imagem = get_field('imagem', $pub);
    $link = get_field('link', $pub);

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <?php if($link) : ?>
        <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
    <?php endif; ?>

        <img src="<?php echo $imagem['url']; ?>" />

    <?php if($link) : ?>
        </a>
    <?php endif; ?>
</div>