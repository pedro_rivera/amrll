<?php

/**
 * Block de Destaque Fixo
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'destaque-fixo-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'destaque-fixo';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$destaques = get_field('destaque');
$listagem = get_field('listagem');
    $criterio = $listagem['criterio'];
    $posts = $listagem['selecao'];
    $posts_d = $listagem['selecao_d'];


$destaque = get_field('destaque_grupo');
$fundo = $destaque['fundo'];
if($fundo == 'outra') :
    $fundo = $destaque['outra_fundo'];
endif;
$texto = $destaque['texto'];
if($texto == 'outra') :
    $texto = $destaque['outra_texto'];
endif;

$conteudo = $destaque['conteudo'];
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>" style="--fundoDestaque: <?php echo $fundo; ?>; --textoDestaque: <?php echo $texto; ?>">
    <?php /*
        $fixo = $destaques;
        include( locate_template( 'cards/fixo.php', false, false ) );
        */
    ?>


    <?php
    if( $conteudo ): ?>
        <div class="slider-fixo">
        <?php foreach( $conteudo as $post ): 

            // Setup this post for WP functions (variable must be named $post).
            setup_postdata($post);
            $fixo = $post;
                include( locate_template( 'cards/fixo.php', false, false ) );
            ?>

        <?php endforeach; ?>
        </div>
        <?php 
        // Reset the global post object so that the rest of the page works correctly.
        wp_reset_postdata(); ?>
    <?php endif; ?>


    <div class="listagem">
    <?php
    if( $criterio == 'fixos' ): ?>
        <?php if($posts) :
            echo "<div>";
            foreach( $posts as $post ): 
                
                // Setup this post for WP functions (variable must be named $post).
                setup_postdata($post);
                $destaque = $post;
                include( locate_template( 'cards/destaque.php', false, false ) );
                
            endforeach;
            // Reset the global post object so that the rest of the page works correctly.
            wp_reset_postdata();
            echo "</div>";
        endif; ?>
        <?php if($posts_d) :
            echo "<div>";
            foreach( $posts_d as $post ): 
                
                // Setup this post for WP functions (variable must be named $post).
                setup_postdata($post);
                $destaque = $post;
                include( locate_template( 'cards/destaque.php', false, false ) );
                
            endforeach;
            // Reset the global post object so that the rest of the page works correctly.
            wp_reset_postdata();
            echo "</div>";
        endif; ?>
    <?php endif; ?>
    </div>
</div>



    <script type="text/javascript">
        
        var imageSlider = tns({
            container: '#<?php echo esc_html($id); ?> .slider-fixo',
            speed: 750,
            items: 1,
            slideBy: 1,
            autoplay: true,
            centerMode: true,
            pauseOnHover: true,
            loop: true,
            nav: true,
            controls: false,
            dots: true,
        });

    </script>