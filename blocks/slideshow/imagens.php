<?php

/**
 * Block Slideshow de Destaque
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'slideshow-imagens-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'slideshow imagens';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
$fundo = get_field('fundo');
$fotos = get_field('fotos');
$texto;

if($fundo == '--preto') {
    $texto = '--branco';
} elseif($fundo == '--branco') {
    $texto = '--preto';
}

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>" style="--fundoSlider: var(<?php echo $fundo; ?>); --texto: var(<?php echo $texto; ?>)">
<?php 
$size = 'slideshow'; // (thumbnail, medium, large, full or custom size)
if( $fotos ): ?>
    <div class="slider">
        <?php foreach( $fotos as $image_id ): ?>
            <div class="foto">
                <div class="prev"><img src="<?php echo get_template_directory_uri(); ?>/img/fotosnav.svg" class="svg"></div>
                <?php echo wp_get_attachment_image( $image_id, $size ); ?>
                <div class="next"><img src="<?php echo get_template_directory_uri(); ?>/img/fotosnav.svg" class="svg"></div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
</div>