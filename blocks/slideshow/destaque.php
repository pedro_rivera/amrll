<?php

/**
 * Block Slideshow de Destaque
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'slideshow-destaque-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'slideshow destaque';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$conteudo = get_field('conteudo');
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
<?php
if( $conteudo ): ?>
    <div class="slider-imagem">
    <?php foreach( $conteudo as $post ): 

        // Setup this post for WP functions (variable must be named $post).
        setup_postdata($post);
        $titulo = str_replace("Amarello Visita: ", "", get_the_title($post->ID));
        $categorias = get_the_category($post->ID);
        $horizontalThumb = get_field('horizontal', $post->ID);
        ?>
            
            <div class="item">
                <?php if($horizontalThumb) { echo wp_get_attachment_image( $horizontalThumb, 'full', '', array('class' => 'bgImage') ); } else { echo get_the_post_thumbnail( $post->ID, 'full', '', array('class' => 'bgImage') ); }; ?>
                <?php
                if(in_category('amarello-visita')) :
                    echo '<a href="'.esc_url( get_category_link( 'amarello-visita' ) ).'" class="categoria">Amarello Visita</a>';
                else : 
                    echo '<a href="'.esc_url( get_category_link( $categorias[0]->term_id ) ).'" class="categoria">' . esc_html( $categorias[0]->name ) . '</a>';
                endif;
                ?>

                <h4><?php echo $titulo; ?></h4>

                <a href="<?php the_permalink($post->ID); ?>" class="leia-mais">Saiba mais</a>
            </div>

    <?php endforeach; ?>
    </div>
    <?php 
    // Reset the global post object so that the rest of the page works correctly.
    wp_reset_postdata(); ?>

    <div class="slider-nav">
        <button class="prev">
            <img src="<?php echo get_template_directory_uri(); ?>/img/seta.svg" class="svg">
        </button>
        <button class="next">
            <img src="<?php echo get_template_directory_uri(); ?>/img/seta.svg" class="svg">
        </button>
    </div>
<?php endif; ?>
</div>

    <script type="text/javascript">
        
        var imageSlider = tns({
            container: '#<?php echo esc_html($id); ?> .slider-imagem',
            speed: 750,
            items: 1,
            slideBy: 1,
            autoplay: false,
            centerMode: true,
            loop: true,
            nav: false,
            controls: true,
            controlsContainer: '#<?php echo esc_html($id); ?> .slider-nav'
        });

    </script>