<?php

/**
 * Block Chamada
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'titulo-completo-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'titulo-completo';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
} 
global $post;

$categorias = get_the_category($post->ID);
$categoriasExibir = get_field('categorias-exibir');
$categoriasTitulo = get_field('categoria-titulo');
$categoriasAutoria = get_field('categoria-autoria');
$autoria = get_field('autoria', $post->ID);
$autoriaExibir = get_field('autoria-exibir');
$autoriaTitulo = get_field('autoria-titulo');
$fonte = get_field('fonte', $post->ID) ?: 'gta';
$edicao = get_field('edicao', $post->ID);

if ( ! is_admin() ) :

?>


<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?> entry-header">
   <?php


		if ( ! empty( $categorias ) && $categoriasExibir == 1 ) {
			echo "<span class='meta ".$categoriasTitulo."-titulo ".$categoriasAutoria."-autoria'>";

            if($edicao) :
                echo "<a href='".get_the_permalink($edicao)."' class='edicao'>#".get_field('numero', $edicao)."</a><a href='".get_the_permalink($edicao)."' class='edicao nome'>".get_the_title($edicao)."</a>";
            endif;
			foreach( $categorias as $category ) {
				$output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '" class="categoria">' . esc_html( $category->name ) . '</a>' . $separator;
			}
			echo trim( $output, $separator );
			echo "</span>";
		}

		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink($post->ID) ) . '" rel="bookmark">', '</a></h2>' );
		endif;
        if( $autoria && $autoriaExibir == 1 ): ?>
            <p class="autoria <?php echo $autoriaTitulo; ?>-titulo">por 
            <?php foreach( $autoria as $post ): 
                setup_postdata($post); ?>
                <a href="<?php echo get_the_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a>
            <?php endforeach; ?>

            <?php if(has_tag('paywall', $padrao->ID)) : ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/lock.svg" class="svg" id="cadeado" alt="Conteúdo exclusivo para assinantes">
            <?php endif; ?>
            </p>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
</div>
            <?php wp_reset_postdata(); ?>

<?php 
else : ?>
<?php if($categoriasExibir == 1) : ?>
    <span class="meta <?php echo $categoriasTitulo; ?>-titulo <?php echo $categoriasAutoria; ?>-autoria"><a href="#" class="edicao"># XX</a> <a href="#" class="edicao">Nome da edição</a> <a href="#" class="categoria">Editoria</a></span>
<?php endif; ?>
    <h2>Título fictício com volume de texto razoável para teste</h2>
<?php if($autoriaExibir) : ?>
    <p class="autoria <?php echo $autoriaTitulo; ?>-titulo">por <a href="#">Autor Desconhecido</a></p>
<?php endif; ?>

<style>
    [data-type="acf/titulo-completo"] .acf-block-preview{display:flex !important;flex-direction:column}[data-type="acf/titulo-completo"] .acf-block-preview.alignleft>*{grid-column:1/-1;text-align:left}[data-type="acf/titulo-completo"] .acf-block-preview.alignleft>.meta{justify-content:flex-start}[data-type="acf/titulo-completo"] .acf-block-preview.alignright>*{grid-column:1/-1;text-align:right}[data-type="acf/titulo-completo"] .acf-block-preview.alignright>.meta{justify-content:flex-end}[data-type="acf/titulo-completo"] .acf-block-preview .antes-titulo{order:-1}[data-type="acf/titulo-completo"] .acf-block-preview .antes-titulo+h2{order:10}[data-type="acf/titulo-completo"] .acf-block-preview .antes-titulo.depois-autoria{order:5}[data-type="acf/titulo-completo"] .acf-block-preview .depois-titulo{order:5}[data-type="acf/titulo-completo"] .acf-block-preview .depois-titulo+h2{order:-1}[data-type="acf/titulo-completo"] .acf-block-preview .depois-titulo.depois-autoria{order:10};
</style>

<?php endif;
?>