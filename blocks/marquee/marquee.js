// get marquee width
function marqueeResize() {
    let marquee = document.querySelector('.marquee p'),
        marqueeWidth = marquee.scrollWidth,
        marqueeWrapper = document.querySelector('.marquee');

    marqueeWrapper.style.setProperty('--width', marqueeWidth + "px");
}

// Attaching the event listener function to window's resize event
window.addEventListener("resize", marqueeResize);

// Calling the function for the first time
marqueeResize();