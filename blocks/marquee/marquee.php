<?php

/**
 * Block Texto Infinito
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'marquee-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'marquee';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$texto = get_field('texto');
$tempo = str_replace(',', '.', get_field('tempo'));
$link = get_field('link');
if($link) :
?>
<a href="<?php echo $link; ?>" id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>" style="--tempo: <?php echo $tempo; ?>s;">
<?php else: ?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>" style="--tempo: <?php echo $tempo; ?>s;">
<?php endif; ?>

        <p><?php echo $texto; ?></p>

<?php if($link) : ?>
</a>
<?php else: ?>
</div>
<?php endif; ?>