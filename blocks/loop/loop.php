<?php

/**
 * Loop de posts
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'loop-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'loop';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$variacao = get_field('variacao');
$className .= ' ' . $variacao;

$dois = get_field('dois');
$tres = get_field('tres');

$fonte = get_field('fonte');
$criterio = get_field('criterio');
$editoria = get_field('editoria');
$autoria = get_field('autoria');

if($variacao == 'quatro') :
    $selecao = get_field('selecao_quatro');
    $className .= ' ' . $fonte;
elseif($variacao == 'lista') :
    $selecao = get_field('selecao_livre');
endif;

if($variacao == 'lista' || $variacao == 'quatro') {
    $className .= ' slideshow';
}

$mostraTitulo = get_field('titulo_exibir');
if($mostraTitulo == 1) {

    if($fonte == 'editoria') : 
        $titulo = $editoria->name;
        $link = home_url()."/editoria/".$editoria->slug;
        
    elseif($fonte == 'autor') :
        $titulo = $autoria->post_title;
        $link = home_url()."/autor/".$autoria->post_name;

    elseif($fonte == 'fixos') :
        $titulo = get_field('titulo');

    endif;

}
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
<?php if($mostraTitulo == 1) : ?>
    <h4 class="titulo">
        <?php if($link) : ?><a href="<?php echo $link; ?>"><?php endif; ?>
            <?php echo $titulo; ?>
        <?php if($link) : ?></a><?php endif; ?>
    </h4>
<?php endif; // mostra titulo?>

<?php 
    if($variacao == 'dois') :
        $showText = false;
        $item01 = $dois['item_1'];
        $item02 = $dois['item_2'];

        $grande = $item01;
        include( locate_template( 'cards/grande.php', false, false ) );
        $grande = $item02;
        include( locate_template( 'cards/grande.php', false, false ) );
    
    elseif($variacao == 'tres') :
        $showText = true;
        $item01 = $tres['item_1'];
        $item02 = $tres['item_2'];
        $item03 = $tres['item_3'];
        echo "<div class='slider'>";
        $vertical = $item01;
        include( locate_template( 'cards/vertical.php', false, false ) );
        $vertical = $item02;
        include( locate_template( 'cards/vertical.php', false, false ) );
        $vertical = $item03;
        include( locate_template( 'cards/vertical.php', false, false ) );
        echo "</div>";
    ?>

    <!-- <div id="float">
        <div class="wrap x">
            <div class="circle y"></div>
        </div>
    </div>         -->

    <?php elseif($variacao == 'quatro' || $variacao == 'lista') : ?>
        <hr>
        <div class="slider">
            <?php if($fonte == 'editoria') : ?>
                <?php
                
                
                if($criterio == 'recentes') :
                    $args = array(
                        'cat' => $editoria->term_id,
                        'posts_per_page' => 12,
                    );
                elseif($criterio == 'lidos') :
                    $args = array(
                        'cat' => $editoria->term_id,
                        'posts_per_page' => 12,
                        'meta_key' => 'post_views_count',
                        'orderby' => 'meta_value_num',
                    );
                elseif($criterio == 'aleatorios') :
                    $args = array(
                        'cat' => $editoria->term_id,
                        'posts_per_page' => 12,
                        'orderby' => 'rand',
                    );
                endif;
                $the_query = new WP_Query( $args ); ?>
                
                <?php if ( $the_query->have_posts() ) : ?>
                    
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                        global $post;
                        $padrao = $post;
                        include( locate_template( 'cards/padrao.php', false, false ) ); ?>
                    <?php endwhile; ?>
                
                    <?php wp_reset_postdata(); ?>
                
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>
            <?php elseif($fonte == 'autor') : ?>
                <?php
                $autor = get_field('autoria', $autoria);
                $autoriaID = array_map(function($o) { return $o; }, $autor);

                $args = array(
                    'post__in' => $autoriaID,
                    'posts_per_page' => 12,
                );
                $the_query = new WP_Query( $args ); ?>
                
                <?php if ( $the_query->have_posts() ) : ?>
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                        global $post;
                        $padrao = $post;
                        include( locate_template( 'cards/padrao.php', false, false ) ); ?>
                    <?php endwhile; ?>
                
                    <?php wp_reset_postdata(); ?>
                
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>
            <?php elseif($fonte == 'fixos') : ?>
                <?php foreach( $selecao as $post ): 
                    setup_postdata($post);
                $padrao = $post;
                include( locate_template( 'cards/padrao.php', false, false ) );
                endforeach; ?>
                <?php 
                // Reset the global post object so that the rest of the page works correctly.
                wp_reset_postdata(); ?>
            <?php endif; ?>

        </div>

    <?php endif; ?>
    
    <?php if($variacao == 'tres' || $variacao == 'quatro' || $variacao == 'lista') : ?>
        <div class="slider-nav">
            <button class="prev">
                <img src="<?php echo get_template_directory_uri(); ?>/img/seta.svg" class="svg">
            </button>
            <button class="next">
                <img src="<?php echo get_template_directory_uri(); ?>/img/seta.svg" class="svg">
            </button>
        </div>
    <?php endif; ?>
    
</div>