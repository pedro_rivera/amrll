<?php
/**
 * Register Blocks
 *
 */
function amarello_register_blocks() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'timeline',
		'title'			=> __( 'Destaque', 'amarello' ),
		'render_template'	=> 'blocks/destaque/destaque.php',
		'enqueue_style' => get_template_directory_uri() . '/blocks/destaque/destaque.css',
		'category'		=> 'design',
		'icon'			=> '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="2" y="2" width="8" height="16" fill="black"/><rect x="11" y="2" width="3" height="2" fill="black"/><rect x="15" y="2" width="3" height="5" fill="black"/><rect x="15" y="8" width="3" height="5" fill="black"/><rect x="15" y="14" width="3" height="4" fill="black"/><rect x="11" y="5" width="3" height="6" fill="black"/><rect x="11" y="12" width="3" height="5" fill="black"/></svg>',
		'align'			=> true,
		'keywords'		=> array( 'destaque', 'scroll' )
	));

	acf_register_block_type( array(
		'name'			=> 'loop',
		'title'			=> __( 'Linha de posts', 'amarello' ),
		'render_template'	=> 'blocks/loop/loop.php',
		'enqueue_style' => get_template_directory_uri() . '/blocks/loop/loop.css',
		'category'		=> 'design',
		'icon'			=> '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="1" y="9" width="6" height="6" fill="black"/><rect x="9" y="9" width="6" height="6" fill="black"/><rect x="17" y="9" width="6" height="6" fill="black"/></svg>
',
		'align'			=> true,
		'keywords'		=> array( 'posts', 'loop', 'linha' )
	));

	acf_register_block_type( array(
		'name'			=> 'publicidade',
		'title'			=> __( 'Publicidade', 'amarello' ),
		'render_template'	=> 'blocks/publicidade/publicidade.php',
		'enqueue_style' => get_template_directory_uri() . '/blocks/publicidade/publicidade.css',
		'category'		=> 'design',
		'icon'			=> '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M11.0756 19.125H13.6744L12.375 15.383L11.0756 19.125ZM24.75 18C23.8198 18 23.0625 18.7573 23.0625 19.6875C23.0625 20.6177 23.8198 21.375 24.75 21.375C25.6802 21.375 26.4375 20.6177 26.4375 19.6875C26.4375 18.7573 25.6802 18 24.75 18ZM32.625 4.5H3.375C1.51172 4.5 0 6.01172 0 7.875V28.125C0 29.9883 1.51172 31.5 3.375 31.5H32.625C34.4883 31.5 36 29.9883 36 28.125V7.875C36 6.01172 34.4883 4.5 32.625 4.5ZM17.6189 24.75H16.4278C15.949 24.75 15.5222 24.4462 15.3647 23.9941L14.8465 22.5H9.90422L9.38531 23.9941C9.30861 24.215 9.16501 24.4064 8.97448 24.5419C8.78396 24.6773 8.55596 24.7501 8.32219 24.75H7.13109C6.35695 24.75 5.81414 23.9871 6.06797 23.2559L9.84375 12.3834C9.95879 12.0523 10.1741 11.7653 10.4597 11.5622C10.7454 11.3591 11.0872 11.2499 11.4377 11.25H13.3123C13.6629 11.2499 14.0048 11.3591 14.2905 11.5624C14.5761 11.7657 14.7914 12.0529 14.9062 12.3841L18.6813 23.2559C18.9352 23.9871 18.3923 24.75 17.6189 24.75V24.75ZM29.8125 23.625C29.8125 24.2466 29.3091 24.75 28.6875 24.75H27.5625C27.2215 24.75 26.9269 24.5904 26.7202 24.3506C26.1141 24.608 25.4482 24.75 24.75 24.75C21.9586 24.75 19.6875 22.4789 19.6875 19.6875C19.6875 16.8961 21.9586 14.625 24.75 14.625C25.3448 14.625 25.9073 14.7466 26.4375 14.9358V12.375C26.4375 11.7534 26.9409 11.25 27.5625 11.25H28.6875C29.3091 11.25 29.8125 11.7534 29.8125 12.375V23.625Z" fill="black"/>
</svg>',
		'align'			=> true,
		'keywords'		=> array( 'ad', 'publicidade', 'propaganda' )
	));

	acf_register_block_type( array(
		'name'			=> 'slideshow-destaques',
		'title'			=> __( 'Slideshow Destaques', 'amarello' ),
		'render_template'	=> 'blocks/slideshow/destaque.php',
		'enqueue_style' => get_template_directory_uri() . '/blocks/slideshow/destaque.css',
		'category'		=> 'design',
		'icon'			=> '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M24 4H0V11H3V13H0V20H24V13H21V11H24V4ZM17 11H7V13H17V11Z" fill="black"/></svg>',
		'align'			=> true,
		'keywords'		=> array( 'slideshow', 'destaques', 'conteudo' )
	));

	acf_register_block_type( array(
		'name'			=> 'chamada',
		'title'			=> __( 'Chamada', 'amarello' ),
		'render_template'	=> 'blocks/chamada/chamada.php',
		'enqueue_style' => get_template_directory_uri() . '/blocks/chamada/chamada.css',
		'category'		=> 'design',
		'icon'			=> '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.00390625 18H3.99991L4 16H2.068V9H4L3.99991 6.99976H0.00390625V18Z" fill="black"/><path d="M15.1473 16H17.7393L13.4913 7.48H10.4673L6.20731 16H8.81132L9.60332 14.284H14.3553L15.1473 16ZM10.3233 12.736L11.4153 10.396C11.5473 10.096 11.8233 9.376 11.9673 8.932H11.9913C12.1353 9.376 12.4113 10.096 12.5433 10.396L13.6353 12.736H10.3233Z" fill="black"/><path d="M23.996 18V7.00009H20V9H21.932V16H20V18H23.996Z" fill="black"/>
</svg>',
		'align'			=> true,
		'keywords'		=> array( 'chamada', 'destaque', 'titulo' )
	));



	acf_register_block_type( array(
		'name'			=> 'marquee',
		'title'			=> __( 'Texto infinito', 'amarello' ),
		'render_template'	=> 'blocks/marquee/marquee.php',
		'enqueue_style' => get_template_directory_uri() . '/blocks/marquee/marquee.css',
		'enqueue_script' => get_template_directory_uri() . '/blocks/marquee/marquee.js',
		'category'		=> 'design',
		'icon'			=> '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M36 13.3895C33.6229 13.9255 32.1681 15.5176 32.1681 17.7399C32.1681 19.9525 33.6229 21.5505 36 22.0892V20.0264C35.1914 19.5547 34.7121 18.749 34.7121 17.7399C34.7121 16.7308 35.1914 15.9251 36 15.4534V13.3895Z" fill="black"/><path d="M0 21.9999V19.0369L2.00407 21.9999H0Z" fill="black"/><path d="M0 18.6192V13.7654C1.27486 14.2417 1.69207 15.2393 1.69207 16.1799C1.69207 17.076 1.26318 18.1222 0 18.6192Z" fill="black"/><path d="M5.88945 20.3079V18.4719H11.8654V16.8879H5.88945V15.1719H12.0694V13.4799H3.46545V21.9999H12.1534V20.3079H5.88945Z" fill="black"/><path d="M22.22 21.9999H14.024V13.4799H16.448V20.1039H22.22V21.9999Z" fill="black"/>
<path d="M23.6569 21.9999H31.8529V20.1039H26.0808V13.4799H23.6569V21.9999Z" fill="black"/><path fill-rule="evenodd" clip-rule="evenodd" d="M36 8H0V7H36V8Z" fill="black"/><path fill-rule="evenodd" clip-rule="evenodd" d="M36 29H0V28H36V29Z" fill="black"/></svg>',
		'align'			=> true,
		'keywords'		=> array( 'marquee', 'destaque', 'titulo' )
	));


	acf_register_block_type( array(
		'name'			=> 'slideshow-imagens',
		'title'			=> __( 'Slideshow de fotos', 'amarello' ),
		'render_template'	=> 'blocks/slideshow/imagens.php',
		'enqueue_style' => get_template_directory_uri() . '/blocks/slideshow/imagens.css',
		'enqueue_script' => get_template_directory_uri() . '/blocks/slideshow/imagens.js',
		'category'		=> 'design',
		'icon'			=> 'dashicons-slides',
		'align'			=> true,
		'keywords'		=> array( 'slideshow', 'fotos', 'galeria', 'imagens' ),
		'align'			=> 'full',
	));

	acf_register_block_type( array(
		'name'			=> 'titulo-completo',
		'title'			=> __( 'Título completo', 'amarello' ),
		'render_template'	=> 'blocks/titulo/completo.php',
		'enqueue_style' => get_template_directory_uri() . '/blocks/titulo/completo.css',
		'category'		=> 'design',
		'icon'			=> '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><g clip-path="url(#clip0)"><path d="M11.439 7.77H1.13V9.811H4.965V17H7.591V9.811H11.439V7.77ZM12.5762 9.551H15.2152V7.549H12.5762V9.551ZM12.6412 17H15.1372V10.5H12.6412V17ZM16.261 12.073H18.172V14.452C18.172 16.324 19.368 17.182 21.63 17.182C22.514 17.182 23.203 17.065 23.606 16.896V15.141C23.294 15.219 22.826 15.297 22.267 15.297C21.188 15.297 20.668 14.894 20.668 13.958V12.073H23.606V10.5H20.668V8.095L18.172 8.979V10.5H16.261V12.073ZM28.4605 17.247C29.9945 17.247 31.3205 16.662 31.9055 15.479V17H34.3625V10.5H31.8665V13.269C31.8665 14.452 31.1125 15.505 29.4485 15.505C28.3175 15.505 27.4465 14.972 27.4465 13.425V10.5H24.9375V14.023C24.9375 16.116 26.2375 17.247 28.4605 17.247ZM39.0567 7.575H36.5607V17H39.0567V7.575ZM45.5334 17.247C48.5624 17.247 50.5774 15.869 50.5774 13.75C50.5774 11.618 48.5624 10.253 45.5334 10.253C42.5044 10.253 40.5024 11.618 40.5024 13.75C40.5024 15.869 42.5044 17.247 45.5334 17.247ZM45.5334 15.648C44.0644 15.648 43.0894 14.907 43.0894 13.75C43.0894 12.593 44.0644 11.852 45.5334 11.852C47.0024 11.852 47.9774 12.593 47.9774 13.75C47.9774 14.907 47.0024 15.648 45.5334 15.648Z" fill="#1B1B19"/><rect x="1" y="1" width="17" height="4" fill="#ADDBE8"/><path d="M2.098 19.594C2.182 19.394 2.308 19.234 2.476 19.114C2.644 18.99 2.848 18.928 3.088 18.928C3.388 18.928 3.63 19.014 3.814 19.186C3.914 19.278 3.986 19.402 4.03 19.558C4.078 19.71 4.102 19.906 4.102 20.146V22H3.592V20.176C3.592 19.644 3.384 19.378 2.968 19.378C2.664 19.378 2.448 19.486 2.32 19.702C2.192 19.918 2.128 20.162 2.128 20.434V22H1.618V19H2.098V19.594ZM6.5807 22.072C6.3047 22.072 6.0607 22.008 5.8487 21.88C5.6367 21.748 5.4727 21.564 5.3567 21.328C5.2407 21.088 5.1827 20.812 5.1827 20.5C5.1827 20.184 5.2407 19.908 5.3567 19.672C5.4727 19.436 5.6367 19.254 5.8487 19.126C6.0607 18.994 6.3047 18.928 6.5807 18.928C6.8567 18.928 7.1007 18.994 7.3127 19.126C7.5247 19.254 7.6887 19.436 7.8047 19.672C7.9207 19.908 7.9787 20.184 7.9787 20.5C7.9787 20.816 7.9207 21.092 7.8047 21.328C7.6887 21.564 7.5247 21.748 7.3127 21.88C7.1007 22.008 6.8567 22.072 6.5807 22.072ZM6.5807 21.628C6.8487 21.628 7.0587 21.53 7.2107 21.334C7.3627 21.134 7.4387 20.856 7.4387 20.5C7.4387 20.148 7.3627 19.872 7.2107 19.672C7.0587 19.472 6.8487 19.372 6.5807 19.372C6.3127 19.372 6.1027 19.472 5.9507 19.672C5.7987 19.868 5.7227 20.144 5.7227 20.5C5.7227 20.856 5.7987 21.134 5.9507 21.334C6.1027 21.53 6.3127 21.628 6.5807 21.628ZM9.19141 19.462C9.25141 19.298 9.33741 19.168 9.44941 19.072C9.56541 18.976 9.69741 18.928 9.84541 18.928C9.99341 18.928 10.1214 18.976 10.2294 19.072C10.3374 19.168 10.4174 19.302 10.4694 19.474C10.5414 19.302 10.6394 19.168 10.7634 19.072C10.8914 18.976 11.0294 18.928 11.1774 18.928C11.3814 18.928 11.5494 19.002 11.6814 19.15C11.8174 19.298 11.8854 19.522 11.8854 19.822V22H11.3754V19.864C11.3754 19.688 11.3414 19.564 11.2734 19.492C11.2054 19.416 11.1214 19.378 11.0214 19.378C10.7014 19.378 10.5414 19.59 10.5414 20.014V22H10.0434V19.864C10.0434 19.692 10.0094 19.568 9.94141 19.492C9.87741 19.416 9.79341 19.378 9.68941 19.378C9.52541 19.378 9.40541 19.436 9.32941 19.552C9.25741 19.664 9.22141 19.818 9.22141 20.014V22H8.71141V19H9.19141V19.462ZM15.2821 20.404C15.2821 20.452 15.2781 20.528 15.2701 20.632H13.1641C13.1641 20.92 13.2421 21.158 13.3981 21.346C13.5581 21.53 13.7741 21.622 14.0461 21.622C14.3901 21.622 14.6541 21.444 14.8381 21.088L15.2701 21.316C15.0141 21.82 14.6061 22.072 14.0461 22.072C13.5861 22.072 13.2361 21.926 12.9961 21.634C12.7601 21.342 12.6421 20.944 12.6421 20.44C12.6421 20.136 12.6981 19.87 12.8101 19.642C12.9221 19.414 13.0801 19.238 13.2841 19.114C13.4921 18.99 13.7341 18.928 14.0101 18.928C14.4461 18.928 14.7661 19.064 14.9701 19.336C15.1781 19.608 15.2821 19.964 15.2821 20.404ZM14.7541 20.212C14.7621 19.956 14.7001 19.744 14.5681 19.576C14.4361 19.408 14.2421 19.324 13.9861 19.324C13.8141 19.324 13.6661 19.366 13.5421 19.45C13.4181 19.534 13.3241 19.644 13.2601 19.78C13.1961 19.916 13.1641 20.06 13.1641 20.212H14.7541Z" fill="#1B1B19"/><path d="M1 22.6H15.8821V22.9H1V22.6Z" fill="#1B1B19"/></g><defs><clipPath id="clip0"><rect width="24" height="24" fill="white"/></clipPath></defs></svg>',
		'align'			=> true,
		'keywords'		=> array( 'titulo', 'completo', 'tags', 'autor' )
	));

}
add_action('acf/init', 'amarello_register_blocks' );

?>