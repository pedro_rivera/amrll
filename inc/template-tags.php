<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Amarello
 */

if ( ! function_exists( 'amarello_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function amarello_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Posted on %s', 'post date', 'amarello' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'amarello_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function amarello_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'amarello' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'amarello_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function amarello_entry_footer() {

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Editar', 'amarello' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'amarello_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function amarello_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

		<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
			<?php
			the_post_thumbnail( 'post-thumbnail', array(
				'alt' => the_title_attribute( array(
					'echo' => false,
				) ),
			) );
			?>
		</a>

		<?php
		endif; // End is_singular().
	}
endif;

if ( ! function_exists( 'wp_body_open' ) ) :
	/**
	 * Shim for sites older than 5.2.
	 *
	 * @link https://core.trac.wordpress.org/ticket/12563
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
endif;



if(!function_exists('amarello_related_posts')) :
	// adds related posts to current
	function amarello_related_posts() {
	global $post;
	$exclude = $post->ID;
	$cat = get_the_category()[0];
	$args = array(
		'post_type' => get_post_type(),
		'posts_per_page' => '12',
		'orderby' => 'rand',
		'post__not_in' => array("$exclude"),
		'cat' => get_the_category()[0]->name,
	);
	$relatedQuery = new WP_Query($args);
	if($relatedQuery->have_posts()) : ?>

		<div class="slideshow loop related-posts">
			
			
			<h3 class="titulo"><?php echo esc_html('Conteúdo relacionado', 'amarello'); ?></h3>
			
			<hr>

			
			<div class="slider">
				<?php
				while($relatedQuery->have_posts()) :
				$relatedQuery->the_post();
				?>
				
				<?php 
					$padrao = $post;
					include( locate_template( 'cards/padrao.php', false, false ) );?>

				<?php endwhile;
				wp_reset_postdata();
				?>
			</div>

			<div class="slider-nav">
				<button class="prev">
					<img src="<?php echo get_template_directory_uri(); ?>/img/seta.svg" class="svg">
				</button>
				<button class="next">
					<img src="<?php echo get_template_directory_uri(); ?>/img/seta.svg" class="svg">
				</button>
			</div>

		</div>

	<?php endif;
	}

endif;




if ( ! function_exists( 'amarello_share_links' ) ) :
// adds share buttons to current page
	function amarello_share_links() {

		$title = get_the_title($post->ID);
		$link = get_the_permalink($post->ID);

		?>
		<div class="share-links">
			<h6>Compartilhar</h6>
			<ul class="links">
				<li><a href="http://twitter.com/share?text=<?php echo bloginfo( 'name' ); ?>&url=<?php echo $link; ?>" target="_blank">Twitter</a></li>
				<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>" target="_blank">Facebook</a></li>
				<li><a href="https://api.whatsapp.com/send?text=<?php echo $title; ?> | <?php echo bloginfo( 'name' ); ?> — <?php echo $link; ?>" target="_blank">WhatsApp</a></li>
			</ul>
		</div>

<?php 
	}

endif;