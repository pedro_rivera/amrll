<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Amarello
 */

get_header();

$terms = get_terms( 'secao');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">


		<?php if ( have_posts() ) : ?>

			<!-- <header class="page-header">
				<h1 class="page-title"><?php echo post_type_archive_title();?></h1>
			</header> -->

			<?php
				$page_id = 4435;  //Page ID
				$page_data = get_page( $page_id ); 

				//store page title and content in variables
				$title = $page_data->post_title; 
				$content = apply_filters('the_content', $page_data->post_content);
			?>

			<?php

			echo $content;
			
			if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
				$count = count( $terms );
				$i = 0;
				$term_list = '<nav class="secoes">';
				foreach ( $terms as $term ) {
					$i++;
					$term_list .= '<a href="' . esc_url( get_term_link( $term ) ) . '" alt="' . esc_attr( sprintf( __( 'Criações da seção %s', 'amarello' ), $term->name ) ) . '">' . $term->name . '</a>';
					if ( $count != $i ) {
						$term_list;
					}
					else {
						$term_list .= '</nav>';
					}
				}
				echo $term_list;
			}

			echo do_shortcode('[ajax_load_more loading_style="infinite classic" container_type="div" post_type="criacao" posts_per_page="12" order="ASC" orderby="menu_order" scroll_distance="-420" button_label="Outras criações" button_loading_label="Carregando criações" button_done_label="Todas as criações exibidas" transition_container_classes="criacoes" archive="true"]');
		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();