<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Amarello
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<?php // cores
			$cores = get_field('cores', 'option');
				$principal = $cores['principal'];
				$complementar = $cores['complementar'];
				$preto = $cores['preto'];
				$branco = $cores['branco'];
				$textoPrincipal = $cores['texto_principal'];
				$textoComplementar = $cores['texto_complementar'];
				$fundo = $cores['fundo'];
				$texto = $cores['texto'];
	?>
	<style>
		:root {
			--principal: <?php echo $principal; ?>;
			--complementar: <?php echo $complementar; ?>;
			--preto: <?php echo $preto; ?>;
			--branco: <?php echo $branco; ?>;
			--textoPrincipal: var(<?php echo $textoPrincipal; ?>);
			--textoComplementar: var(<?php echo $textoComplementar; ?>);
			--fundo: var(<?php echo $fundo; ?>);
			--texto: var(<?php echo $texto; ?>);
		}
	</style>
	<?php
	$logos = get_field( 'logos', 'option' );
		$index = array_rand( $logos );
		$rand_row = $logos[ $index ];
		$logo = $rand_row['opcao'];
		$align = $rand_row['menu'];
	?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'amarello' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="custom-logo">
				<img src="<?php echo $logo; ?>" alt="<?php bloginfo( 'name' ); ?>" class="svg">
			</a>
		</div><!-- .site-branding -->

			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><img src="<?php echo get_template_directory_uri(); ?>/img/menu.svg" class="svg menu-icon" alt="Abrir menu"></button>
			<nav>
				<div class="wrapper">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-2',
						'menu_id'        => 'editorias',
						'container'		 => false,
					) );
					echo "<div class='topo'>";
						echo "<div class='login-wrapper'>";
							wp_nav_menu( array(
								'theme_location' => 'menu-6',
								'menu_id'		 => false,
								'container'		 => false,
							) );
						echo "</div>";
						dynamic_sidebar( 'topo' );
					echo "</div>";
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'paginas',
						'container'		 => false,
					) );
					?>
				</div>
			</nav>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
