<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Amarello
 */
$categorias = get_the_category();
$autoria = get_field('autoria');
$patrocinado = get_field('patrocinado');
$texto = get_field('patrocinado-texto');
$link = get_field('patrocinado-link');
$fonte = get_field('fonte') ?: 'gta';
$edicao = get_field('edicao');

?>

<article id="post-<?php the_ID(); ?>" <?php post_class($fonte); ?>>
<?php if(!has_block('acf/titulo-completo')) : ?>
	<header class="entry-header">
		<?php


		if ( ! empty( $categorias ) ) {
			echo "<span class='meta'>";

            if($edicao) :
                echo "<a href='".get_the_permalink($edicao)."' class='edicao'>#".get_field('numero', $edicao)."</a><a href='".get_the_permalink($edicao)."' class='edicao nome'>".get_the_title($edicao)."</a>";
            endif;
			foreach( $categorias as $category ) {
				$output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '" class="categoria">' . esc_html( $category->name ) . '</a>' . $separator;
			}
			echo trim( $output, $separator );
			echo "</span>";
		}

		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
        if( $autoria ): ?>
            <p class="autoria">por 
            <?php foreach( $autoria as $post ): 
                setup_postdata($post); ?>
                <a href="<?php echo get_the_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a>
            <?php endforeach; ?>

            <?php if(has_tag('paywall', $padrao->ID)) : ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/lock.svg" class="svg" id="cadeado" alt="Conteúdo exclusivo para assinantes">
            <?php endif; ?>
            </p>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
	</header><!-- .entry-header -->
	<?php endif; ?>

	<div class="entry-content">
		<?php if($patrocinado) : ?>
			<a id="patrocinado" target="_blank" href="<?php echo $link; ?>"><?php echo $texto; ?></a>
		<?php endif; ?>
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'amarello' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );
		amarello_share_links();
		?>
	</div><!-- .entry-content -->

	<?php
		amarello_related_posts();
	?>

	<footer class="entry-footer">
		<?php amarello_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->