<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Amarello
 */
// $categorias = get_the_term_list( $taxonomy = 'tipo' );
$tipo = get_the_terms( get_the_ID(), 'tipo' );
$secao = get_the_terms( get_the_ID(), 'secao' );
$fonte = get_field('fonte') ?: 'gta';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class($fonte); ?>>
	<header class="entry-header">
		<?php


		if ( ! empty( $tipo ) || ! empty( $secao ) ) {
			echo "<span class='meta'>";
			foreach( $secao as $category ) {
				$output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '" class="categoria">' . esc_html( $category->name ) . '</a>' . $separator;
			}
			foreach( $tipo as $category ) {
				$output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '" class="categoria">' . esc_html( $category->name ) . '</a>' . $separator;
			}
			echo trim( $output, $separator );
			echo "</span>";
		}

		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'amarello' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );
		amarello_share_links();
		?>
	</div><!-- .entry-content -->

	<?php
		amarello_related_posts();
	?>

	<footer class="entry-footer">
		<?php amarello_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
