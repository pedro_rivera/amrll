<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Amarello
 */
$posts = get_field('autoria');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header vcard">
			<?php
			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;
			if( '' !== get_post()->post_content ) {
				echo "<div class='bio'>";
					the_content();
				echo "</div>";
			}
			if(have_rows('links')) :
				echo "<ul class='links'>";
					while(have_rows('links')) : the_row();
					$link = get_sub_field('link');
					echo "<li><a href='".esc_url($link['url'])."' target='".esc_attr($link['target'])."'>".esc_html($link['title'])."</a></li>";
					endwhile;
				echo "</ul>";
			endif;
			?>

	</header><!-- .entry-header -->

	<?php if ($posts) : ?>
		<h3>Conteúdo da pessoa</h3>
	<?php endif; ?>
	<div class="entry-content artigos">
		
		<?php
			// the_content();
			

			if($posts) :
				foreach( $posts as $post ): 

					// Setup this post for WP functions (variable must be named $post).
					setup_postdata($post);
					$padrao = $post;
					include( locate_template( 'cards/padrao.php', false, false ) );
				endforeach;
			// Reset the global post object so that the rest of the page works correctly.
			wp_reset_postdata();
			endif;
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php amarello_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
