<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Amarello
 */
$publicacao = get_field('publicacao');
$impressa = get_field('impressa');
$destaque = get_field('destaque');
$posts = get_field('edicao');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header outdoor">
		<div class="info">
			<?php if($publicacao) : ?>
			<p class="publicacao"><?php echo $publicacao; ?></p>
			<?php endif; ?>
			<?php
			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif; ?>
			<?php if($impressa) : ?>
				<a class="button" href="<?php echo $impressa; ?>">Comprar versão impressa</a>
			<?php endif; ?>
		</div>
	<?php if($destaque) : ?>
		<div class="post-thumbnail">
			<img src="<?php echo $destaque; ?>" alt="<?php echo the_title(); ?>">
		</div>
	<?php else :
		echo amarello_post_thumbnail();
	endif; ?>
	</header><!-- .entry-header -->

	<?php if ($posts) : ?>
		<h3>Conteúdo completo</h3>
	<?php endif; ?>

	<div class="entry-content artigos">
		
		<?php
			the_content();

			if($posts) :
				foreach( $posts as $post ): 

					// Setup this post for WP functions (variable must be named $post).
					setup_postdata($post);
					$padrao = $post;
					include( locate_template( 'cards/padrao.php', false, false ) );
				endforeach;
			// Reset the global post object so that the rest of the page works correctly.
			wp_reset_postdata();
			endif;
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php amarello_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
