<?php
// campos
$autoria = get_field('autoria', $destaque->ID);
$edicao = get_field('edicao', $destaque->ID);
$categorias = get_the_category($destaque->ID);
$thumb = get_the_post_thumbnail( $destaque->ID, 'medium' );
?>
<div class="card padrao destaque">
    <a href="<?php echo get_the_permalink($destaque->ID); ?>"><?php echo $thumb; ?></a>
    <div class="info">
        <a href="<?php echo get_the_permalink($destaque->ID); ?>" class="titulo">
            <h2><?php echo get_the_title($destaque->ID); ?></h2>
        </a>

        <span class="meta">
            <?php if($edicao) : ?>
                <a href="<?php echo get_the_permalink($edicao); ?>" class="edicao">#<?php echo the_field('numero', $edicao); ?></a>
                <a href="<?php echo get_the_permalink($edicao); ?>" class="edicao nome"><?php echo get_the_title($edicao); ?></a>
            <?php endif; ?>

            <?php echo '<a href="'.esc_url( get_category_link( $categorias[0]->term_id ) ).'" class="categoria">' . esc_html( $categorias[0]->name ) . '</a>';?>
        </span>
        
        <?php
        if( $autoria ): ?>
            <p class="autoria">por 
            <?php foreach( $autoria as $post ): 
                setup_postdata($post); ?>
                <a href="<?php echo get_the_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a>
            <?php endforeach; ?>

            <?php if(has_tag('paywall', $destaque->ID)) : ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/lock.svg" class="svg" id="cadeado" alt="Conteúdo exclusivo para assinantes">
            <?php endif; ?>
            </p>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    </div>
</div>