<?php
// campos
$ano = get_field('ano', $fixo->ID);
$estacao = get_field('estacao', $fixo->ID);
$numero = get_field('numero', $fixo->ID);
$vertical = get_field('vertical', $fixo->ID);
$horizontal = get_field('horizontal', $fixo->ID);
$thumb = get_the_post_thumbnail( $fixo->ID, 'full' );
$categorias = get_the_category($fixo->ID);
?>

<div class="card fixo">
    <a href="<?php echo get_the_permalink($fixo->ID); ?>"><?php if($horizontalThumb) { echo wp_get_attachment_image( $horizontalThumb, 'full', '', array() ); } else { echo get_the_post_thumbnail( $fixo->ID, 'full' ); }; ?>
        <div class="info">
            <h2 class="titulo"><?php echo $fixo->post_title; ?></h2>
                <span class="meta"><?php if($ano) : ?><span class="ano">Ano <?php echo $ano; ?></span><?php endif; ?><?php if($numero) : ?><span class="numero">N.<?php echo $numero; ?></span><?php endif; ?><?php if($estacao) : ?><span class="estacao"><?php echo $estacao; ?></span><?php endif; ?><?php if('post' == get_post_type($fixo->ID)) : ?><?php echo '<span class="categoria">' . esc_html( $categorias[0]->name ) . '</span>';?><?php endif; ?></span>
            <?php if(has_excerpt($post->ID)) : ?>
            <p><?php echo get_the_excerpt($fixo->ID); ?></p>
            <?php endif; ?>
        </div>
    </a>
</div>