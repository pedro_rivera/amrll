<?php
// campos
$autoria = get_field('autoria', $editoria->ID);
$edicao = get_field('edicao', $editoria->ID);
$categorias = get_the_category($editoria->ID);
$horizontalThumb = get_field('horizontal', $editoria->ID);
$verticalThumb = get_field('vertical', $editoria->ID);
?>
<div class="card editoria padrao">
    <a href="<?php echo get_the_permalink($editoria->ID); ?>"><?php if($horizontalThumb) { echo wp_get_attachment_image( $horizontalThumb, 'medium', '', array() ); } else { echo get_the_post_thumbnail( $editoria->ID, 'medium' ); }; ?></a>
    <div class="info">
        <a href="<?php echo get_the_permalink($editoria->ID); ?>" class="titulo">
            <!-- <pre>
                <?php // print_r($editoria); ?>
            </pre> -->
            <h2><?php echo get_the_title($editoria->ID); ?></h2>
        </a>

        <span class="meta">
            <?php if($edicao) : ?>
                <a href="<?php echo get_the_permalink($edicao); ?>" class="edicao">#<?php echo the_field('numero', $edicao); ?></a>
                <a href="<?php echo get_the_permalink($edicao); ?>" class="edicao nome"><?php echo get_the_title($edicao); ?></a>
            <?php endif; ?>

            <?php echo '<a href="'.esc_url( get_category_link( $categorias[0]->term_id ) ).'" class="categoria">' . esc_html( $categorias[0]->name ) . '</a>';?>
        </span>
        
        <?php
        if( $autoria ): ?>
            <p class="autoria">por 
            <?php foreach( $autoria as $post ): 
                setup_postdata($post); ?>
                <a href="<?php echo get_the_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a>
            <?php endforeach; ?>

            <?php if(has_tag('paywall', $editoria->ID)) : ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/lock.svg" class="svg" id="cadeado" alt="Conteúdo exclusivo para assinantes">
            <?php endif; ?>
            </p>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>


        <div class="resumo">
            <?php echo get_the_excerpt($editoria->ID); ?>
        </div>
    </div>
</div>