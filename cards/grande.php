<?php
// campos
$autoria = get_field('autoria', $grande->ID);
$edicao = get_field('edicao', $grande->ID);
$categorias = get_the_category($grande->ID);
$verticalThumb = get_field('vertical', $grande->ID);
$horizontalThumb = get_field('horizontal', $grande->ID);
$thumb = get_the_post_thumbnail( $grande->ID, 'medium' );
$separator = ' ';
$output = '';
?>
<div class="card grande">
    <a href="<?php echo get_the_permalink($grande->ID); ?>"><?php if($horizontalThumb) { echo wp_get_attachment_image( $horizontalThumb, 'medium', '', array() ); } else { echo get_the_post_thumbnail( $grande->ID, 'medium' ); }; ?></a>
    <div class="info">
        <span class="meta">
            <?php
                if ( ! empty( $categorias ) ) {
                    foreach( $categorias as $category ) {
                        $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '" class="categoria">' . esc_html( $category->name ) . '</a>' . $separator;
                    }
                    echo trim( $output, $separator );
                }
            ?>
        </span>

        <a href="<?php echo get_the_permalink($grande->ID); ?>" class="titulo">
            <h2><?php echo $grande->post_title; ?></h2>
        </a>
        
        <?php if($showText = true) : ?>
            <div class="resumo">
                <?php echo get_the_excerpt($grande->ID); ?>
            </div>
        <?php endif; ?>

        <?php
        if( $autoria ): ?>
            <p class="autoria">por 
            <?php foreach( $autoria as $post ): 
                setup_postdata($post); ?>
                <a href="<?php echo get_the_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a>
            <?php endforeach; ?>

            <?php if(has_tag('paywall', $grande->ID)) : ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/lock.svg" class="svg" id="cadeado" alt="Conteúdo exclusivo para assinantes">
            <?php endif; ?>
            </p>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    </div>
</div>