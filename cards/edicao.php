<?php
    $publicacao = get_field('publicacao');
?>

<div class="card edicao">
    <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_post_thumbnail(); ?></a>
    <div class="info">
        <a href="<?php echo get_the_permalink(); ?>" class="titulo">
            <h2><?php echo the_title(); ?></h2>
        </a>
        <p class="publicacao"><?php echo $publicacao; ?></p>
    </div>
</div>