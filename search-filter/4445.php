<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>
	
	Encontrados <?php echo $query->found_posts; ?> resultados<br />
	<div class='search-filter-results-list resultados'>
	<?php
		while ($query->have_posts())
		{
			$query->the_post();
            // campos
            $autoria = get_field('autoria');
            $edicao = get_field('edicao');
            $categorias = get_the_category();
            ?>
            <div class="card padrao">
                <a href="<?php echo get_the_permalink(); ?>"><?php echo the_post_thumbnail('thumbnail'); ?></a>
                <div class="info">
                    <a href="<?php echo get_the_permalink(); ?>" class="titulo">
                        <!-- <pre>
                            <?php // print_r($padrao); ?>
                        </pre> -->
                        <h2><?php echo get_the_title(); ?></h2>
                    </a>

                    <span class="meta">
                        <?php if($edicao) : ?>
                            <a href="<?php echo get_the_permalink($edicao); ?>" class="edicao">#<?php echo the_field('numero', $edicao); ?></a>
                            <a href="<?php echo get_the_permalink($edicao); ?>" class="edicao nome"><?php echo get_the_title($edicao); ?></a>
                        <?php endif; ?>

                        <?php echo '<a href="'.esc_url( get_category_link( $categorias[0]->term_id ) ).'" class="categoria">' . esc_html( $categorias[0]->name ) . '</a>';?>
                    </span>
                    
                    <?php
                    if( $autoria ): ?>
                        <p class="autoria">por 
                        <?php foreach( $autoria as $post ): 
                            setup_postdata($post); ?>
                            <a href="<?php echo get_the_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a>
                        <?php endforeach; ?>

                        <?php if(has_tag('paywall')) : ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/lock.svg" class="svg" id="cadeado" alt="Conteúdo exclusivo para assinantes">
                        <?php endif; ?>
                        </p>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
			
			<?php
		}
	?>
	</div>
<?php
}
else
{
	?>
	<div class='search-filter-results-list' data-search-filter-action='infinite-scroll-end'>
		<span>End of Results</span>
	</div>
	<?php
}
?>