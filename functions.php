<?php
/**
 * Amarello functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Amarello
 */

if ( ! function_exists( 'amarello_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function amarello_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Amarello, use a find and replace
		 * to change 'amarello' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'amarello', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Páginas', 'amarello' ),
			'menu-2' => esc_html__( 'Editorias', 'amarello' ),
			'menu-3' => esc_html__( 'Visita', 'amarello' ),
			'menu-4' => esc_html__( 'Loja', 'amarello' ),
			'menu-5' => esc_html__( 'Redes Sociais', 'amarello' ),
			'menu-6' => esc_html__( 'Login', 'amarello' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'amarello_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'amarello_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function amarello_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'amarello_content_width', 640 );
}
add_action( 'after_setup_theme', 'amarello_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function amarello_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'amarello' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'amarello' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Menu superior', 'amarello' ),
		'id'            => 'topo',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Newsletter', 'amarello' ),
		'id'            => 'newsletter',
		'description'   => esc_html__( 'Widget de newsletter.', 'amarello' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Disclaimer rodapé', 'amarello' ),
		'id'            => 'disclaimer',
		'description'   => esc_html__( 'Widget de texto.', 'amarello' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'amarello_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function amarello_scripts() {
	wp_enqueue_style( 'amarello-style', get_stylesheet_uri() );

	wp_enqueue_script( 'amarello-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	
	wp_enqueue_script( 'amarello-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	
	if(is_singular()){
		//We only want the script if it's a singular page
		$id = get_the_ID();
		if(has_block('acf/slideshow-destaques')){
			wp_enqueue_script( 'slider', 'https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js' );
			wp_enqueue_style( 'slider', 'https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css' );
		}
		if(has_block('acf/loop',$id) || has_block('acf/slideshow-imagens') || 'product' == get_post_type() || 'post' == get_post_type() || 'criacao' == get_post_type()){
			wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', true );
			wp_enqueue_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );
		}
	}
	
	if(is_category()) {
		wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', true );
		wp_enqueue_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );
	}

	wp_enqueue_script( 'amarello-scripts', get_template_directory_uri() . '/js/amarello.js', array(), '20200702', true );
}
add_action( 'wp_enqueue_scripts', 'amarello_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Load custom blocks
 */
require get_template_directory() . '/inc/blocks.php';


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Personalização',
		'menu_title'	=> 'Personalização',
		'menu_slug' 	=> 'personalizacao',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Visual',
		'menu_title'	=> 'Visual',
		'parent_slug'	=> 'personalizacao',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'SEO',
		'menu_title'	=> 'SEO',
		'parent_slug'	=> 'personalizacao',
	));
	
}

/**
 * Custom WC Markups
 */
if (!function_exists('woo_custom_product_block_markup')) {
	function woo_custom_product_block_markup($data) {
		$product_markup = new DOMDocument();
		$product_markup->loadHTML($data);
		$product_anchor = $product_markup->getElementsByTagName('a');
		$product_id = '';

		foreach ($product_anchor as $anchor) {
			if ($anchor !== '') {
				$product_id = $anchor->getAttribute('data-product_id');
			}
		}

		$product = wc_get_product($product_id);
		// more product data...

		return '
			<li class="' . implode(' ', wc_get_product_class('', $product)) . ' wc-block-grid__product">
				<a href="'.$product->get_permalink().'" class="wc-block-grid__product-link">
					<div class="wc-block-grid__product-image">' . $product->get_image( 'woocommerce_thumbnail' ) . '</div>
					<div class="wc-block-grid__product-title"><h3>' . get_the_title($product_id) . '</h3></div>
				</a>
				<div class="wc-block-grid__product-price price">'.$product->get_price_html().'</div>
				<div class="wp-block-button wc-block-grid__product-add-to-cart"><a href="'.$product->get_permalink().'">Assinar</a></div>
			</li>
		';
	}
}
add_filter('woocommerce_blocks_product_grid_item_html', 'woo_custom_product_block_markup');

// custom single product markup
// remove breadcrumbs
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
// remove sidebar
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

// put single summary after add to cart button
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 35);

// remove single meta
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

// remove product description and aditional info
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);

// wc login shortcode
function my_render_wc_login_form( $atts ) { 
  if ( ! is_user_logged_in() ) {  
    if ( function_exists( 'woocommerce_login_form' ) && 
        function_exists( 'woocommerce_output_all_notices' ) ) {
      //render the WooCommerce login form   
      ob_start();
      woocommerce_output_all_notices();
	  woocommerce_login_form();
	  echo "<a href='".home_url( '/assine' )."'>Cadastre-se</a>";
      return ob_get_clean();
    } else { 
      //render the WordPress login form
      return wp_login_form( array( 'echo' => false ));
    }
  }
}
add_shortcode( 'my_wc_login_form', 'my_render_wc_login_form' );

add_action('wp_logout','auto_redirect_after_logout');

function auto_redirect_after_logout(){

  wp_redirect( home_url() );
  exit();

}

// custom paywall message

function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos   = array_keys($words);
        $text  = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

add_filter('leaky_paywall_subscribe_or_login_message', 'zeen101_custom_subscribe_content', 10, 3 );

function zeen101_custom_subscribe_content( $new_content, $message, $content ) {
	
	$length = get_field('preview-paywall', 'option');
	$settings = get_leaky_paywall_settings();
	$lead_in = str_replace( $message, '', $new_content);
	$resumo = wp_trim_words( $content, $length );
	$thumbnail = get_the_post_thumbnail();

	$subscribe_nag  = '<div class="conteudo-exclusivo">';

	// your custom subscribe nag message goes here
	$subscribe_nag .= '<h3>Este conteúdo é exclusivo para assinantes</h3><p class="mensagem"><a href="'.home_url( "/assine" ).'">Assine</a> ou <a class="login-trigger" href="javascript:document.querySelector(\'body\').classList.toggle(\'open-login\')">faça login</a> para ter acesso a todo o nosso conteúdo.</p>'; 
	
	$subscribe_nag .= '</div>';

	// return do_shortcode( $lead_in . $subscribe_nag );
	return $thumbnail . '<p class="resumo-paywall">' . $resumo . '</p>' . $subscribe_nag;
}

function my_product_carousel_options($options) {
  $options['animation'] = 'fade';
  $options['smoothHeight'] = true;
  return $options;
}
add_filter("woocommerce_single_product_carousel_options", "my_product_carousel_options", 10);

// botão de continuar comprando
add_action('woocommerce_cart_coupon', 'continuar_comprando');
// add_action( 'woocommerce_cart_actions', 'themeprefix_back_to_store' );

function continuar_comprando() { ?>
<a class="button wc-backward" href="<?php echo home_url( 'feira' ); ?>"> <?php _e( 'Continuar comprando', 'woocommerce' ) ?> </a>

<?php
}

// add_action( 'after_setup_theme', 'gutenCss' );
 
// function gutenCss(){
 
// 	add_theme_support( 'editor-styles' ); // if you don't add this line, your stylesheet won't be added
// 	add_editor_style( 'guten.css' ); // tries to include style-editor.css directly from your theme folder
 
// }

function paul_guten_block_editor_assets() {
	wp_enqueue_style(
		'paul-editor-style',
		get_stylesheet_directory_uri() . "/editor.css",
		array(),
		'1.0'
	);
}
add_action('enqueue_block_editor_assets', 'paul_guten_block_editor_assets');