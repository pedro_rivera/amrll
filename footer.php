<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Amarello
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'paginas',
				'container'		 => false,
			) );
			dynamic_sidebar( 'disclaimer' );
			wp_nav_menu( array(
				'theme_location' => 'menu-5',
				'menu_id'        => 'redes-sociais',
				'container'		 => false,
			) );
			wp_nav_menu( array(
				'theme_location' => 'menu-2',
				'menu_id'        => 'editorias',
				'container'		 => false,
			) );
			wp_nav_menu( array(
				'theme_location' => 'menu-3',
				'menu_id'        => 'visita',
				'container'		 => false,
			) );
			dynamic_sidebar( 'newsletter' );
			dynamic_sidebar( 'busca' );
		?>
	</footer><!-- #colophon -->
	
	<?php echo "<div class='login-form'>".do_shortcode('[my_wc_login_form]')."</div>"; ?>
	<div class="login-overlay"></div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
