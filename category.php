<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Amarello
 */

get_header();


	$term = get_queried_object();

	// vars
	$criterio = get_field('criterio', $term);

	if($criterio == 'fixos') {
		$ativar = get_field('destaque-ativar', $term);
		$full = get_field('destaque', $term);
		if(!$full) {
			$full = array();
		}
		$topo = get_field('topo', $term);
		$destaques = array_values($topo); 
		$listagem = get_field('primeira_listagem', $term);
		$not__in = array_merge($topo, $listagem, $full);
		$alm_not__in = implode(', ', $not__in);

		$dArgs = array(
			'post__in' => $destaques,
			'posts_per_page' => '-1',
				'orderby' =>  'post__in',
			
		);

		$dQuery = new WP_Query ($dArgs);

		if($listagem) {
			$nArgs = array(
				'post__in' => $listagem,
				'posts_per_page' => '-1',
				'orderby' =>  'post__in',
			);

			$nQuery = new WP_Query ($nArgs);
		}

	}

	$args = array(
		'posts_per_page' => '3',
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => "'".$term->slug."'",
			),
		),
	);

	$query = new WP_Query ($args);
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main <?php echo $criterio; ?>">

		<!-- <pre>
			<?php // print_r($query); ?>
		</pre> -->

		<header class="page-header">
			<?php
			the_archive_title( '<h1 class="editoria-title">', '</h1>' );
			the_archive_description( '<div class="archive-description">', '</div>' );
			?>
			<!-- <pre>
				<?php print_r($not__in); ?>
			</pre> -->
		</header><!-- .page-header -->

		<?php
		if( $full ): ?>
			<div class="destaque full">
			<?php foreach( $full as $post ): 

			// Setup this post for WP functions (variable must be named $post).
			setup_postdata($post);
			$titulo = str_replace("Amarello Visita: ", "", get_the_title($post->ID));
			$categorias = get_the_category($post->ID);
			?>
            
            <div class="item">
                <img src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" alt="<?php echo $titulo; ?>" class="bgImage">
                <?php
                if(in_category('amarello-visita')) :
                    echo '<a href="'.esc_url( get_category_link( 'amarello-visita' ) ).'" class="categoria">Amarello Visita</a>';
                else : 
                    echo '<a href="'.esc_url( get_category_link( $categorias[0]->term_id ) ).'" class="categoria">' . esc_html( $categorias[0]->name ) . '</a>';
                endif;
                ?>

                <h4><?php echo $titulo; ?></h4>

                <a href="<?php the_permalink($post->ID); ?>" class="leia-mais">Saiba mais</a>
            </div>
			<?php endforeach; ?>
			</div>
			<?php 
			// Reset the global post object so that the rest of the page works correctly.
			wp_reset_postdata(); ?>
		<?php endif; ?>

		<div class="block loop tres">
			<div class="slider">
			<?php if( $criterio == 'fixos' ) : ?>
				<?php
					while( $dQuery->have_posts() ) :
						$dQuery->the_post();
						// global $post;
						$vertical = $post;
						include( locate_template( 'cards/vertical.php', false, false ) );
						
					endwhile;
					wp_reset_postdata();
				?>
			<?php elseif( $criterio !== 'fixos' ) : ?>
				<?php
					while( $query->have_posts() ) :
						$query->the_post();
						$do_not_duplicate[] = $post->ID;
						// global $post;
						$vertical = $post;
						include( locate_template( 'cards/vertical.php', false, false ) );
					endwhile;
					wp_reset_postdata();
					$alm_not__in = ($do_not_duplicate) ? implode(',', $do_not_duplicate) : '';
				?>
			<?php endif; ?>
				</div>
				<div class="slider-nav">
					<button class="prev">
						<img src="<?php echo get_template_directory_uri(); ?>/img/seta.svg" class="svg">
					</button>
					<button class="next">
						<img src="<?php echo get_template_directory_uri(); ?>/img/seta.svg" class="svg">
					</button>
				</div>
			</div>
		<!-- <ol class="listagem"> -->
		<section class="listagem">
		<?php if($criterio == 'fixos') :

			if($nQuery) :
				while ( $nQuery->have_posts() ) :
					$nQuery->the_post();
					$editoria = $post;
					// echo "<li>".get_the_title()."</li>";
        			include( locate_template( 'cards/editoria.php', false, false ) );

				endwhile;
				wp_reset_postdata();
			endif;
		endif; ?>
		<?php
			echo do_shortcode('[ajax_load_more loading_style="grey" post_type="post" posts_per_page="12" post__not_in="'.$alm_not__in.'" images_loaded="true" scroll_container=".listagem" transition_container="false" archive="true" button_label="Carregar mais" button_loading_label="Carregando"]');

			include( locate_template( 'blocks/newsletter/newsletter.php', false, false ) );
		?>
		</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
